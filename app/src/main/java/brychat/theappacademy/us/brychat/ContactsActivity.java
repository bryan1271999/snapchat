package brychat.theappacademy.us.brychat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ContactsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ContactsFragment contactsFragment = new ContactsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, contactsFragment).commit();
    }

}
