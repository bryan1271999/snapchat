package brychat.theappacademy.us.brychat;


public class Constants {

    public static final String ACTION_ADD_FRIEND = "brychat.theappacademy.us.brychat.ADD_FRIEND";
    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "brychat.theappacademy.us.brychat.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "brychat.theappacademy.us.brychat.ADD_FRIEND_FAILURE";
    public static final String ACTION_SEND_FRIEND_REQUEST = "brychat.theappacademy.us.brychat.SEND_FRIEND_REQUEST";
    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "brychat.theappacademy.us.brychat.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "brychat.theappacademy.us.brychat.FRIEND_REQUEST_FAILURE";
    public static final String ACTION_SEND_PICTURE = "brychat.theappacademy.us.brychat.ACTION_SEND_PICTURE";

}
