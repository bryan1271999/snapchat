package brychat.theappacademy.us.brychat;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import java.util.ArrayList;

public class Theme extends AppCompatActivity {

    public static String color;
    public static ArrayList<FrameLayout> layouts;
    public static ArrayList<Activity> activities;

    public static void InitializeArrays() {
        layouts = new ArrayList<>();
        activities = new ArrayList<>();
    }

    public static void ChangeColor() {
        if(color != null) {
            for(int x = 0; x < layouts.size(); x++) {
                if(color.equals("Blue") && layouts.get(x) != null) {
                    layouts.get(x).setBackgroundColor(Color.BLUE);
                    layouts.get(x).refreshDrawableState();

                }
                else if(color.equals("Red") && layouts.get(x) != null) {
                    layouts.get(x).setBackgroundColor(Color.RED);
                    layouts.get(x).refreshDrawableState();
                }
                else if(color.equals("Yellow") && layouts.get(x) != null) {
                    layouts.get(x).setBackgroundColor(Color.YELLOW);
                    layouts.get(x).refreshDrawableState();
                }
            }
        }
    }

}
