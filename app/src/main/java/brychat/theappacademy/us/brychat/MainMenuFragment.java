package brychat.theappacademy.us.brychat;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


public class MainMenuFragment extends Fragment {

    public static Activity mainActivity;
    public static FrameLayout mainLayout;


    public MainMenuFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        Theme.ChangeColor();
        mainActivity = getActivity();
        mainLayout = (FrameLayout) getActivity().findViewById(R.id.container);
        Theme.layouts.add(mainLayout);
        Theme.activities.add(mainActivity);
        Theme.ChangeColor();
        String[] menuItems = {"", "Contacts", "Messages", "Profile", "Settings", "Camera", "Friend Requests"};
        ListView listView = (ListView) view.findViewById(R.id.mainMenu);
        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<>(
            getActivity(),
            android.R.layout.simple_list_item_1,
            menuItems
        );
        listView.setAdapter(listViewAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    Intent intent = new Intent(getActivity(), ContactsActivity.class);
                    startActivity(intent);
                }
                else if (position == 2) {
                    Intent intent = new Intent(getActivity(), MessageActivity.class);
                    startActivity(intent);
                }
                else if(position == 3) {
                    Intent intent = new Intent(getActivity(), ProfileActivity.class);
                    startActivity(intent);
                }
                else if(position == 4) {
                    Intent intent = new Intent(getActivity(), SettingActivity.class);
                    startActivity(intent);
                }
                else if(position == 5) {
                    Intent intent = new Intent(getActivity(), CameraActivity.class);
                    startActivity(intent);
                }
                else if(position == 6) {
                    Intent intent = new Intent(getActivity(), FriendRequestActivity.class);
                    startActivity(intent);
                }
            }
        });
        Button logOutButton = (Button) view.findViewById(R.id.logOutButton);
        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Backendless.UserService.logout(new AsyncCallback<Void>() {
                    @Override
                    public void handleResponse(Void response) {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(), "There Was an Issue With Logging Out", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        return view;
    }

}
