package brychat.theappacademy.us.brychat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;

public class ContactsFragment extends Fragment {

    private ArrayList<String> friendList;
    private ArrayAdapter<String> arrayAdapter;
    private String currentUserName;
    public static FrameLayout contactsLayout;

    public ContactsFragment() {

    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        contactsLayout = (FrameLayout) getActivity().findViewById(R.id.container);
        Theme.layouts.add(contactsLayout);
        Theme.ChangeColor();
        final Uri imageToSend = getActivity().getIntent().getParcelableExtra("ImageUri");
        final boolean picture = getActivity().getIntent().getBooleanExtra("pictureBoolean", false);
        Button searchButton = (Button) view.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Add Friend");
                final EditText inputField = new EditText(getActivity());
                alertDialog.setView(inputField);
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.setPositiveButton("Add Friend", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getActivity(), SearchActivity.class);
                        intent.putExtra("FriendName", inputField.getText().toString());
                        startActivity(intent);
                    }
                });
                alertDialog.create();
                alertDialog.show();
            }
        });
        ListView listView = (ListView) view.findViewById(R.id.friendList);
        friendList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, friendList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (picture == false) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setMessage("Get Picture From...");
                    dialog.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(getActivity(), CameraActivity.class);
                            startActivity(intent);
                        }
                    });
                    dialog.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
                        }
                    });
                    dialog.setNeutralButton("Sent Photos", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(getActivity(), BackendlessPhotoActivity.class);
                            startActivityForResult(intent, 1);
                        }
                    });
                    dialog.create();
                    dialog.show();
                }
                else if(picture == true){
                    String friendName = (String) parent.getItemAtPosition(position);
                    sendImage(currentUserName, friendName, imageToSend);
                    CameraFragment cameraFragment = new CameraFragment();
                    GalleryFragment galleryFragment = new GalleryFragment();
                    if(cameraFragment.cameraActivity != null) {
                        cameraFragment.cameraActivity.finish();
                    }
                    if(galleryFragment.galleryActivity != null) {
                        galleryFragment.galleryActivity.finish();
                    }
                    getActivity().finish();
                }
            }
        });
        String currentUser = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                currentUserName = (String) user.getProperty("name");
                Object[] friendObjects = (Object[]) user.getProperty("friends");
                if(friendObjects.length > 0) {
                    BackendlessUser[] friendArray = (BackendlessUser[]) friendObjects;
                    for(BackendlessUser friend : friendArray) {
                        String name = friend.getProperty("name").toString();
                        friendList.add(name);
                        arrayAdapter.notifyDataSetChanged();
                    }
                }
            }
            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
        return view;
    }

    private void sendImage(String currentUser, String toUser, Uri imageUri) {
        Intent intent = new Intent(getActivity(), SnapchatService.class);
        intent.setAction(Constants.ACTION_SEND_PICTURE);
        intent.putExtra("fromUser", currentUser);
        intent.putExtra("toUser", toUser);
        intent.putExtra("imageUri", imageUri);
        getActivity().startService(intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == getActivity().RESULT_OK) {
                Uri uri = data.getData();
                Intent intent = new Intent(getActivity(), ContactsActivity.class);
                intent.putExtra("ImageUri", uri);
                intent.putExtra("pictureBoolean", true);
                startActivity(intent);
            }
            else if(resultCode == getActivity().RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
