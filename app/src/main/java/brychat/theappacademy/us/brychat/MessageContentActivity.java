package brychat.theappacademy.us.brychat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class MessageContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_content);
        MessageContentFragment messageContentFragment = new MessageContentFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.messageContentContainer, messageContentFragment).commit();
    }
}
