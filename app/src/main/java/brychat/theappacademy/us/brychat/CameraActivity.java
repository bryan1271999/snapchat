package brychat.theappacademy.us.brychat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CameraFragment cameraFragment = new CameraFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, cameraFragment).commit();
    }
}
