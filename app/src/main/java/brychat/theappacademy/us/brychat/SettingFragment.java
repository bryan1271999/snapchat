package brychat.theappacademy.us.brychat;


import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class SettingFragment extends Fragment {

    private ArrayAdapter<String> arrayAdapter;
    private String[] options = {"Background"};
    public static FrameLayout settingLayout;
    public static Activity settingActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        settingLayout = (FrameLayout) getActivity().findViewById(R.id.container);
        settingActivity = getActivity();
        Theme.activities.add(settingActivity);
        Theme.layouts.add(settingLayout);
        Theme.ChangeColor();
        ListView listView = (ListView) view.findViewById(R.id.settings);
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, options);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setMessage("Set Background Color");
                    ListView color = new ListView(getActivity());
                    String[] colors = {"Blue", "Red", "Yellow"};
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, colors);
                    dialog.setView(color);
                    color.setAdapter(adapter);
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialog.create();
                    dialog.show();
                    color.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if(position == 0) {
                                Theme.color = "Blue";
                                Theme.ChangeColor();
                            }
                            else if(position == 1) {
                                Theme.color = "Red";
                                Theme.ChangeColor();
                            }
                            else if(position == 2) {
                                Theme.color = "Yellow";
                                Theme.ChangeColor();
                            }
                        }
                    });
                }
            }
        });
        return view;
    }

}
