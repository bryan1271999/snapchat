package brychat.theappacademy.us.brychat;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class BackendlessPhotoFragment extends Fragment {

    private ArrayList<Bitmap> photoArray;
    private ArrayAdapter<Bitmap> arrayAdapter;


    public BackendlessPhotoFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_backendless_photo, container, false);
        photoArray = new ArrayList<>();
        ListView listView = (ListView) view.findViewById(R.id.photoList);
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, photoArray);
        listView.setAdapter(arrayAdapter);
        return view;
    }

}
