package brychat.theappacademy.us.brychat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class SettingActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SettingFragment settingFragment = new SettingFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, settingFragment).commit();
    }
}
