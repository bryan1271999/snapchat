package brychat.theappacademy.us.brychat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;
import java.util.Iterator;

public class SearchFragment extends Fragment {

    private String search;
    private ArrayList<String> friendList;
    private ArrayAdapter<String> arrayAdapter;
    public static FrameLayout searchLayout;

    public SearchFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        searchLayout = (FrameLayout) getActivity().findViewById(R.id.container);
        Theme.layouts.add(searchLayout);
        Theme.ChangeColor();
        ListView listView = (ListView) view.findViewById(R.id.search);
        friendList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, friendList);
        search = getActivity().getIntent().getStringExtra("FriendName");
        getUsers();
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SendFriendRequest(friendList.get(position));
            }
        });
        return view;
    }

    public void SendFriendRequest(final String friendName) {
        String currentUserId = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUserId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser currentUser) {
                Intent intent = new Intent(getActivity(), SnapchatService.class);
                intent.setAction(Constants.ACTION_SEND_FRIEND_REQUEST);
                intent.putExtra("fromUser", currentUser.getProperty("name").toString());
                intent.putExtra("toUser", friendName);
                getActivity().startService(intent);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }

    private void getUsers() {
        Backendless.Data.of(BackendlessUser.class).find(new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> response) {
                for(int x = 0; x < response.getData().size(); x++) {
                    String user = response.getData().get(x).getProperty("name").toString();
                    for(int y = 0; y < user.length() - search.length() + 1; y++) {
                        if(user.substring(y, y + search.length()).equals(search)) {
                            friendList.add(user);
                        }
                    }
                }
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }

}
