package brychat.theappacademy.us.brychat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class ProfileFragment extends Fragment {

    private ArrayAdapter<String> arrayAdapter;
    private String[] profileOptions = {"Change Username"};
    public static FrameLayout profileLayout;

    public ProfileFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile ,container, false);
        profileLayout = (FrameLayout) getActivity().findViewById(R.id.container);
        Theme.layouts.add(profileLayout);
        Theme.ChangeColor();
        ListView listView = (ListView) view.findViewById(R.id.profile);
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, profileOptions);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    final EditText editText = new EditText(getActivity());
                    dialog.setView(editText);
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialog.setPositiveButton("Change Username", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String newName = editText.getText().toString();
                            if(newName.length() > 0) {
                                ChangeUserName(newName);
                            }
                        }
                    });
                    dialog.create();
                    dialog.show();
                }
            }
        });
        return view;
    }

    private void ChangeUserName(final String newName) {
        Backendless.Persistence.of(BackendlessUser.class).findById(Backendless.UserService.loggedInUser(), new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                response.setProperty("name", newName);
                Backendless.UserService.update(response, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Log.i("Saved", "Changes");
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {

                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }

}
