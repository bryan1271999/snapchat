package brychat.theappacademy.us.brychat;

public class SentPicture {

    private boolean viewed;
    private String fromUser;
    private String toUser;
    private String imageLocation;

    public SentPicture() {
        viewed = false;
        fromUser = "";
        toUser = "";
        imageLocation = "";
    }

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean wasViewed) {
        viewed = wasViewed;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }

}
