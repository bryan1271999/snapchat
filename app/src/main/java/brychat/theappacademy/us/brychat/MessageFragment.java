package brychat.theappacademy.us.brychat;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MessageFragment extends Fragment {

    private ArrayList<String> messages;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<SentPicture> incomingPhotos;
    public static FrameLayout messageLayout;

    public MessageFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        Theme.ChangeColor();
        messageLayout = (FrameLayout) getActivity().findViewById(R.id.container);
        Theme.layouts.add(messageLayout);
        Theme.ChangeColor();
        messages = new ArrayList<>();
        incomingPhotos = new ArrayList<>();
        ListView listView = (ListView) view.findViewById(R.id.messages);
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, messages);
        listView.setAdapter(arrayAdapter);
        String currentUser = Backendless.UserService.loggedInUser();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayImageFromFriend(position);
            }
        });
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                String currentUserName = (String) user.getProperty("name");
                getPhotosSentTo(currentUserName);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
        return view;
    }

    private void displayImageFromFriend(int position) {
        String imageLocation = incomingPhotos.get(position).getImageLocation();
        try {
            URL url = new URL("https://api.backendless.com/363A690B-C713-F1F9-FFC0-3AF431499F00/v1/files/" + imageLocation);
            DownloadFilesTask task = new DownloadFilesTask();
            task.execute(url);
        }
        catch(MalformedURLException exception) {
            exception.printStackTrace();
        }
    }

    private void getPhotosSentTo(String username) {
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(String.format("toUser = '%s'", username));
        Backendless.Persistence.of(SentPicture.class).find(query, new AsyncCallback<BackendlessCollection<SentPicture>>() {
            @Override
            public void handleResponse(BackendlessCollection<SentPicture> response) {
                List<SentPicture> photos = response.getData();
                for(SentPicture photo: photos) {
                    if(!photo.isViewed()) {
                        messages.add(photo.getFromUser());
                        incomingPhotos.add(photo);
                    }
                }
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }

    private void displayPopupImage(Bitmap bitmap) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setMessage("Incoming Photo");
        dialog.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ImageView imageView = new ImageView(getActivity());
        imageView.setImageBitmap(bitmap);
        dialog.setView(imageView);
        dialog.create();
        dialog.show();
    }

    private class DownloadFilesTask extends AsyncTask<URL, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(URL... params) {
            for(URL url : params) {
                try {
                    HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                    int responseCode = httpCon.getResponseCode();
                    if(responseCode == HttpURLConnection.HTTP_OK) {
                        InputStream inputStream = httpCon.getInputStream();
                        Bitmap imageBitmap = BitmapFactory.decodeStream(inputStream);
                        inputStream.close();
                        return imageBitmap;
                    }
                }
                catch(IOException exception) {
                    exception.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            displayPopupImage(bitmap);
        }
    }

}
