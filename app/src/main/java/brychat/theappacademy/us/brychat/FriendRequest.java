package brychat.theappacademy.us.brychat;


public class FriendRequest {

    String toUser;
    String fromUser;
    boolean accepted;

    public FriendRequest() {
        toUser = "";
        fromUser = "";
        accepted = false;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String user) {
        toUser = user;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String user) {
        fromUser = user;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean isAccepted) {
        accepted = isAccepted;
    }

}
