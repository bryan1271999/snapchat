package brychat.theappacademy.us.brychat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BackendlessPhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BackendlessPhotoFragment backendlessPhotoFragment = new BackendlessPhotoFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, backendlessPhotoFragment).commit();
    }
}
