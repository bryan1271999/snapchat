package brychat.theappacademy.us.brychat;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;
import java.util.List;

public class FriendRequestFragment extends android.support.v4.app.Fragment {

    private ListView requestList;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> requestStrings;
    private ArrayList<FriendRequest> friendRequests;
    public static FrameLayout friendRequestLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_request, container, false);
        friendRequestLayout = (FrameLayout) getActivity().findViewById(R.id.container);
        Theme.layouts.add(friendRequestLayout);
        Theme.ChangeColor();
        requestStrings = new ArrayList<>();
        friendRequests = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, requestStrings);
        requestList = (ListView) view.findViewById(R.id.requestList);
        requestList.setAdapter(arrayAdapter);
        requestList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showAcceptDialog(position);
            }
        });
        String user = Backendless.UserService.loggedInUser();
        Backendless.UserService.findById(user, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                String currentUserName = response.getProperty("name").toString();
                getIncomingFriendRequests(currentUserName);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
        return view;
    }

    private void showAcceptDialog(final int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Accept friend request from " + requestStrings.get(position) + "?");
        alertDialog.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                acceptRequest(friendRequests.get(position));
            }
        });
        alertDialog.create();
        alertDialog.show();
    }

    private void acceptRequest(final FriendRequest request) {
        request.setAccepted(true);
        Backendless.Persistence.save(request, new AsyncCallback<FriendRequest>() {
            @Override
            public void handleResponse(FriendRequest response) {
                Intent intent = new Intent(getActivity(), SnapchatService.class);
                intent.setAction(Constants.ACTION_ADD_FRIEND);
                intent.putExtra("firstUserName", request.getFromUser());
                intent.putExtra("secondUserName", request.getToUser());
                getActivity().startService(intent);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }

    private void getIncomingFriendRequests(String userName) {
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(String.format("toUser = '%s'", userName));
        Backendless.Persistence.of(FriendRequest.class).find(query, new AsyncCallback<BackendlessCollection<FriendRequest>>() {
            @Override
            public void handleResponse(BackendlessCollection<FriendRequest> response) {
                List<FriendRequest> incomingRequests = response.getData();
                for(int x = 0; x < incomingRequests.size(); x++) {
                    if(incomingRequests.get(x).isAccepted() == false) {
                        requestStrings.add(incomingRequests.get(x).getFromUser());
                        friendRequests.add(incomingRequests.get(x));
                    }
                }
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
    }

}
