package brychat.theappacademy.us.brychat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {
    public static final String APP_ID = "363A690B-C713-F1F9-FFC0-3AF431499F00";
    public static final String SECRET_KEY = "0A2BE4A5-4062-C502-FF19-3D0115915500";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Theme.layouts == null) {
            Theme.InitializeArrays();
        }
        if(Backendless.UserService.loggedInUser() == "") {
            LoginMenuFragment loginMenuFragment = new LoginMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loginMenuFragment).commit();
        }
        else {
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        }
    }
}
