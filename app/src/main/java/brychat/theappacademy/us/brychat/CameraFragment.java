package brychat.theappacademy.us.brychat;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraFragment extends Fragment {

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    private String imageFilePath;
    private Uri thePicture;
    public static Activity cameraActivity;
    public static FrameLayout cameraLayout;


    public CameraFragment() {

    }

    public File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd__HHmmss").format(new Date());
        String fileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(fileName, ".jpg", storageDir);
        return image;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        cameraLayout = (FrameLayout) getActivity().findViewById(R.id.container);
        Theme.layouts.add(cameraLayout);
        Theme.ChangeColor();
        cameraActivity = getActivity();
        File imageFile = null;
        try {
            imageFile = createImageFile();
            imageFilePath = imageFile.getAbsolutePath();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
        if(imageFile != null) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
            if(intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }
        }
        return view;
    }

    private void addPhotoToGallery(String filePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(filePath);
        Uri uri = Uri.fromFile(f);
        thePicture = uri;
        mediaScanIntent.setData(uri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_IMAGE_CAPTURE) {
            if(resultCode == Activity.RESULT_OK) {
                addPhotoToGallery(imageFilePath);
                Intent intent = new Intent(getActivity(), ContactsActivity.class);
                intent.putExtra("ImageUri", thePicture);
                intent.putExtra("pictureBoolean", true);
                startActivity(intent);
            }
        }
    }

}
